<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Notification;
use App\Notifications\VendorApproveNotification;

class AdminController extends Controller
{
    public function AdminDashboard(){
        return view('admin.index');
    }

    public function AdminProfile(){
        $id = Auth::user()->id;
        $adminData = User::find($id);
        return view('admin.profile.index',compact('adminData'));
    }

    public function AdminStore(Request $request){
        $id = Auth::user()->id;
        $data = User::find($id);
        $data->username = $request->username;
        $data->phone = $request->phone;
        $data->email = $request->email;
        $data->address = $request->address;
        if($request->file('image')){
            $file = $request->file('image');
            @unlink(public_path('upload/adminimg/'.$data->image));
            $filename = date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('upload/adminimg'),$filename);
            $data['image'] = $filename;
        }
        $data->save();

        $notification = array(
            'message' => 'Admin updated successfully!!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function AdminPassword(){
        return view('admin.profile.password');
    }

    public function updatePassword(Request $request){
        $validateData = $request->validate([
            'oldpassword' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password'
        ]);

        $hashPassword = Auth::user()->password;
        if(Hash::check($request->oldpassword,$hashPassword)){
            $users = User::find(Auth::id());
            $users->password = bcrypt($request->new_password);
            $users->save();

            session()->flash('message','Password change successfully!');
            return redirect('logout');
        }else{
            session()->flash('message','Oops something went wrong!');
            return redirect()->back();
        }

    }

    //Login 
    public function AdminLogin(){
        return view('admin.login');
    }

    //Logout 
    public function AdminDestroy(Request $request)
    {
        Auth::guard('web')->logout();
        return redirect()->route('admin.login');
    }


     /*************
      * Use for role & permission start
      */
      public function AllAdmin(){
        $allAdmin = User::where('role','admin')->get();
        return view('backend.admin.all_admin',compact('allAdmin'));
      }

      public function AddAdmin(){
        $roles = Role::all();
        return view('backend.admin.add_admin',compact('roles'));
      }

      public function roleWiseAdminStore(Request $request){
        $data = new User();
        $data->name = $request->name;
        $data->username = $request->username;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->address = $request->address;
        $data->password = Hash::make($request->password);
        $data->role = 'admin';
        $data->status = 'active';
        $data->save();

        if($request->roles){
            $data->assignRole($request->roles);
        }
        $notification = array(
            'message' => 'New Admin User Inserted Successfully',
            'alert-type' => 'success'
        );

        return redirect()->route('all.admin')->with($notification);

      }

      public function roleWiseAdminEdit($id){
        $roles = Role::all();
        $editData = User::findOrFail($id);
        return view('backend.admin.edit_admin',compact('roles','editData'));
      }

      public function roleWiseAdminUpdate(Request $request, $id){
        $data = User::findOrFail($id);
        $data->name = $request->name;
        $data->username = $request->username;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->address = $request->address;
        $data->role = 'admin';
        $data->status = 'active';
        $data->update();

        $data->roles()->detach();
        if($request->roles){
            $data->assignRole($request->roles);
        }
        $notification = array(
            'message' => 'New Admin User Update Successfully',
            'alert-type' => 'success'
        );

        return redirect()->route('all.admin')->with($notification);
      }

      public function roleWiseAdminDelete($id){
        $data = User::findOrFail($id);
        if(!is_null($data)){
            $data->delete();
        }

        $notification = array(
            'message' => 'Admin User deleted Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
      }

}
