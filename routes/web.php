<?php

use App\Models\User;
use App\Http\Middleware\RedirectIfAuthenticated;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProfileController;

/**********Backend Controller**************/
use App\Http\Controllers\Backend\ProductController;


/**********Frontend Controller**************/

use App\Http\Controllers\Frontend\IndexController;
use App\Http\Controllers\Frontend\UserController;
use App\Http\Controllers\Frontend\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// Route::get('/',[IndexController::class,'Index']);

// Route::get('/dashboard', function () {
//     $id = Auth::user()->id;
//     $userData = User::find($id);
//     return view('frontend.userprofile.dashboard',compact('userData'));
// })->middleware(['auth', 'verified'])->name('dashboard');

// Route::middleware('auth')->group(function () {
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::post('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
//     Route::post('/user/password', [ProfileController::class, 'userUpdatePassword'])->name('user_update_password');
// });

require __DIR__.'/auth.php';

//Admin middleware
Route::middleware(['auth', 'role:admin'])->group(function () {
    //Admin Dashboard
    Route::controller(AdminController::class)->group(function () {
        Route::get('/admin/dashboard', 'AdminDashboard')->name('admin.dashboard');
        Route::get('/logout', 'AdminDestroy')->name('admin.logout');
        Route::get('/admin/profile','AdminProfile')->name('admin_profile');
        Route::post('/admin/store','AdminStore')->name('admin_store');
        Route::get('/admin/password','AdminPassword')->name('password_change');
        Route::post('/update/password','updatePassword')->name('update_password');
    });


    //Product routing
    Route::controller(ProductController::class)->prefix('products')->group(function () {
        Route::get('/view', 'view')->name('view_product');
        Route::get('/add', 'add')->name('add_product');
        Route::post('/store', 'store')->name('store_product');
        Route::get('/edit/{id}', 'edit')->name('edit_product');
        Route::post('/update/{id}', 'update')->name('update_product');
        Route::get('/delete/{id}', 'delete')->name('delete_product');
        //main image update
        Route::post('/main-img/{mainimg_update}','mainImgUpdate')->name('update_mainimg');
        Route::post('/multi-img','multiImgUpdate')->name('multiimg_update');
        Route::get('/multi-delete/{id}','multiImgUDelete')->name('remove_multimg');
        //Json data for Category to SubCategory
        Route::get('/get/subcategory/{category_id}', 'VendorGetSubCategory');
        Route::get('/active/product', 'ProductActive')->name('product_active');
         //Details product
        Route::get('/details/{id}', 'ProductDetails')->name('details_product');
        //Stock product
        Route::get('/product-stock', 'StockProduct')->name('product_stock');
    });

});  //End Admin middleware

Route::get('admin/login', [AdminController::class, 'AdminLogin'])->name('admin.login')->middleware(RedirectIfAuthenticated::class);
Route::get('/get/inactive/{id}', [ProductController::class, 'GetInActive']);
Route::get('/get-active/{id}', [ProductController::class, 'GetActive'])->name('get_active');


Route::get('/product-details/{id}/{slug}',[IndexController::class,'ProductDetails']);


Route::get('/category-details/{id}/{slug}',[IndexController::class,'CategoryWiseProduct']);
Route::get('/subcategory-product/{id}/{slug}',[IndexController::class,'SubCategoryWiseProduct']);
Route::get('/product-modal-view/{id}',[IndexController::class,'proModalView']);
//Search route
Route::post('/search',[IndexController::class,'productSearch'])->name('product_search');
Route::post('/search-product',[IndexController::class,'advanceSearch']);


