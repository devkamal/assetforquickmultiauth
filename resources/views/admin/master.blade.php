<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--favicon-->
      <link rel="icon" href="{{ asset('backend/assets/images/favicon-32x32.png') }}" type="image/png" />
      <!--plugins-->
      <link href="{{ asset('backend/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet"/>
      <link href="{{ asset('backend/assets/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet" />
      <link href="{{ asset('backend/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" />
      <link href="{{ asset('backend/assets/plugins/metismenu/css/metisMenu.min.css') }}" rel="stylesheet" />
      <!-- loader-->
      <link href="{{ asset('backend/assets/css/pace.min.css') }}" rel="stylesheet" />
      <script src="{{ asset('backend/assets/js/pace.min.js') }}"></script>
      <!-- Bootstrap CSS -->
      <link href="{{ asset('backend/assets/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ asset('backend/assets/css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('backend/assets/css/icons.css') }}" rel="stylesheet">
      <!-- Theme Style CSS -->
      <link rel="stylesheet" href="{{ asset('backend/assets/css/dark-theme.css') }}" />
      <link rel="stylesheet" href="{{ asset('backend/assets/css/semi-dark.css') }}" />
      <link rel="stylesheet" href="{{ asset('backend/assets/css/header-colors.css') }}" />
       <!-- Toastr CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
      <title>Dashboard</title>
      <!-- DataTable -->
      <link href="{{ asset('backend/assets/plugins/datatable/css/dataTables.bootstrap5.min.css') }}" rel="stylesheet" />
      <!-- DataTable-->
      <link href="{{ asset('backend/assets/plugins/input-tags/css/tagsinput.css') }}" rel="stylesheet" />
      <!-- Jquery-->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
   </head>
   <body>
      <!--wrapper-->
      <div class="wrapper">
         <!--sidebar wrapper -->
         @include('admin.sidebar')
         <!--end sidebar wrapper -->
         <!--start header -->
         @include('admin.header')
         <!--end header -->
         <!--start page wrapper -->
         @yield('content')
         <!--end page wrapper -->
         <!--start overlay-->
         @include('admin.footer')
      </div>
      <!--end wrapper-->
      <!--start switcher-->
         @include('admin.switcher')
      <!--end switcher-->

      <!-- Bootstrap JS -->
      <script src="{{ asset('backend/assets/js/bootstrap.bundle.min.js') }}"></script>
      <!--plugins-->
      <script src="{{ asset('backend/assets/js/jquery.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/simplebar/js/simplebar.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/metismenu/js/metisMenu.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/chartjs/js/Chart.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/sparkline-charts/jquery.sparkline.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/jquery-knob/excanvas.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/jquery-knob/jquery.knob.js') }}"></script>
      <script src="{{ asset('backend/assets/js/index.js') }}"></script>
       <!--Validate JS-->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>
      <!--app JS-->
      <script src="{{ asset('backend/assets/js/app.js') }}"></script>
      <!-- Toastr JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
      <!-- Tostr js -->
      <!--Datatable-->
      <script src="{{ asset('backend/assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
      <script>
            $(document).ready(function() {
               $('#example').DataTable();
            } );
      </script>
      <!--tagsinput-->
      <script src="{{ asset('backend/assets/plugins/input-tags/js/tagsinput.js') }}"></script>
      <!--tinymce editor-->
      <script src='https://cdn.tiny.cloud/1/vdqx2klew412up5bcbpwivg1th6nrh3murc6maz8bukgos4v/tinymce/5/tinymce.min.js' referrerpolicy="origin">
      </script>
      <script>
         tinymce.init({
         selector: '#mytextarea'
         });
      </script>
      <!--tinymce editor-->

      <script>
         $(function() {
          $(".knob").knob();
         });
      </script>

      <script>
         @if(Session::has('message'))
         var type = "{{ Session::get('alert-type','info') }}"
         switch(type){
            case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
            case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
            case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
            case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
         }
         @endif
      </script>

      <!-------Sweet Alert-------->
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
      <script type="text/javascript">
         $(function(){
            $(document).on('click','#delete',function(e){
               e.preventDefault();
               var link = $(this).attr("href");
               Swal.fire({
                  title: 'Are you sure?',
                  text: "Delete this data!",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, delete it!'
               }).then((result) => {
                  if (result.isConfirmed) {
                     window.location.href = link;
                     Swal.fire(
                     'Deleted!',
                     'Your file has been deleted successfully.',
                     'success'
                     )
                  }
               });
            });
         });
      </script>


   </body>
</html>