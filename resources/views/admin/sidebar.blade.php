<div class="sidebar-wrapper" data-simplebar="true">
   <div class="sidebar-header">
      <div>
         <img src="{{asset('backend/logo.png')}}" class="logo-icon" alt="logo icon">
      </div>
   
      <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
      </div>
   </div>

   <!--navigation-->
   <ul class="metismenu" id="menu">
      <li>
         <a href="{{ route('admin.dashboard') }}">
            <div class="parent-icon"><i class='bx bx-home-circle'></i>
            </div>
            <div class="menu-title">Dashboard</div>
         </a>
      </li>

 

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Export Bill</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_product') }}"><i class="bx bx-right-arrow-alt"></i>Add New Export Bill</a></li>

            <li> <a href="{{ route('view_product') }}"><i class="bx bx-right-arrow-alt"></i>Export Bill List</a></li>
         </ul>
      </li>

   
      <li>
         <a href="https://webbysys.com/" target="_blank">
            <div class="parent-icon"><i class="bx bx-support"></i>
            </div>
            <div class="menu-title">Support</div>
         </a>
      </li>
      <br><br><br><br><br><br>
   </ul>
   <!--end navigation-->
</div>