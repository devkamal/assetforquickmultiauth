@extends('frontend.layouts.master')
@section('content')
<main class="main pages">
   <div class="page-header breadcrumb-wrap">
      <div class="container">
         <div class="breadcrumb">
            <a href="index.html" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
            <span></span> Pages <span></span> My Account
         </div>
      </div>
   </div>
   <div class="page-content pt-150 pb-150">
      <div class="container">
         <div class="row">
            <div class="">
               <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-8">
                     <div class="login_wrap widget-taber-content background-white">
                        <div class="padding_eight_all bg-white">
                           <div class="heading_s1">
                              <h1 class="mb-5">Create an Account</h1>
                              <p class="mb-30">Already have an account? <a href="{{ route('login') }}">Login</a></p>
                           </div>
                           <form method="POST" action="{{ route('register') }}">
                              @csrf
                              <div class="form-group">
                                 <input type="text" required="" name="name" placeholder="Username" id="name">
                                 @error('name')
                                 <span class="text-danger">{{ $message }}</span>
                                 @enderror
                              </div>
                              <div class="form-group">
                                 <input type="email" required="" name="email" id="email" placeholder="Email">
                                 @error('email')
                                 <span class="text-danger">{{ $message }}</span>
                                 @enderror
                              </div>
                              <div class="form-group">
                                 <input required="" type="password" id="password" name="password" placeholder="Password">
                                 @error('password')
                                 <span class="text-danger">{{ $message }}</span>
                                 @enderror
                              </div>
                              <div class="form-group">
                                 <input required="" type="password" id="password_confirmation" name="password_confirmation" placeholder="Confirm password">
                                 @error('password')
                                 <span class="text-danger">{{ $message }}</span>
                                 @enderror
                              </div>
                              <div class="form-group">
                                 <button type="submit" class="btn btn-heading btn-block hover-up form-control loginbtn" name="login">Submit &amp; Register</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</main>
@endsection