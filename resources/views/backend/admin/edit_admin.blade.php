@extends('admin.master')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<div class="page-wrapper">
      <div class="page-content"> 
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                  <div class="breadcrumb-title pe-3">RoleWise Admin update</div>
                  <div class="ps-3">
                        <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">RoleWise Admin updatep</li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                          <a href="{{ route('all.admin') }}">Back</a>
                                    </li>
                              </ol>
                        </nav>
                  </div>
            </div>
            <!--end breadcrumb-->
            <div class="container">
                  <div class="main-body">
                        <div class="row">
                              <div class="col-lg-12">
                                    <form action="{{ route('rolewise.udpdate.admin',$editData->id) }}" method="post">
                                          @csrf
                                          <div class="card">
                                                <div class="card-body">
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">User Name</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="name" class="form-control" value="{{ $editData->name }}">
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Full Name</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="username" class="form-control" value="{{ $editData->username }}">
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Email</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="email" class="form-control" value="{{ $editData->email }}">
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Phone</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="phone" class="form-control" value="{{ $editData->phone }}">
                                                            </div>
                                                      </div>
                                                
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Address</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="address" class="form-control" value="{{ $editData->address }}">
                                                            </div>
                                                      </div>

                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Assign Roles</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                 <select name="roles" id="roles" class="form-control">
                                                                  <option value="">User Roles</option>
                                                                  @foreach ($roles as $role)
                                                                        <option value="{{ $role->id }}" {{ $editData->hasRole($role->name)?'selected':''}}>{{ $role->name }}</option>
                                                                  @endforeach
                                                                 </select>
                                                            </div>
                                                      </div>

                                                      <div class="row">
                                                            <div class="col-sm-3"></div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="submit" class="btn btn-primary px-4" value="Save Changes">
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </form>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>


@endsection