@extends('admin.master')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<div class="page-wrapper">
      <div class="page-content"> 
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                  <div class="breadcrumb-title pe-3">SubCategory</div>
                
               
            </div>
            <!--end breadcrumb-->
            <div class="container">
                  <div class="main-body">
                        <div class="row">
                        <div class="d-flex justify-content-between">
                              <p class="mb-0 text-uppercase">Update Sub->Category</p>
                              <a href="{{ route('view_subcategory') }}" class="mb-0 text-uppercase btn btn-primary btn-sm">Back</a>
                        </div>
    
                              <div class="col-lg-12">
                                    <form action="{{ route('update_subcategory',$editData->id) }}" method="post">
                                          @csrf
                                          <div class="card">
                                                <div class="card-body">
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Category Name</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <div class="col-sm-9 text-secondary form-group">
                                                                        <select name="category_id" id="category_id" class="form-control">
                                                                              <option value="">Select Category</option>
                                                                              @foreach ($categories as $item)
                                                                                    <option value="{{ $item->id }}" {{ $item->id == $editData->category_id ? 'selected' : ''}}>{{ $item->name }}</option>
                                                                              @endforeach
                                                                        </select>
                                                                        @error('category_id')
                                                                              <span style="color:red">{{ $message }}</span>
                                                                        @enderror
                                                                  </div>
                                                            </div>
                                                      </div>

                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Sub Category Name</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="subcategory_name" class="form-control" value="{{ $editData->subcategory_name }}">
                                                                  @error('subcategory_name')
                                                                        <span style="color:red">{{ $message }}</span>
                                                                  @enderror
                                                            </div>
                                                      </div>

                                                      <div class="row">
                                                            <div class="col-sm-3"></div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="submit" class="btn btn-primary px-4" value="Save Changes">
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </form>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>


@endsection
