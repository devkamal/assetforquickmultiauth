@extends('admin.master')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<div class="page-wrapper">
<div class="page-content"> 
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Sub->Category</div>
            <div class="ps-3">
                  <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                              <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                              </li>
                              <li class="breadcrumb-item active" aria-current="page">Sub->Category</li>
                        </ol>
                  </nav>
            </div>
       
      </div>
      <!--end breadcrumb-->
      <div class="container">
            <div class="main-body">
                  <div class="row">
                  <div class="d-flex justify-content-between">
                        <p class="mb-0 text-uppercase">Add Sub->Category</p>
                        <a href="{{ route('view_subcategory') }}" class="mb-0 text-uppercase btn btn-primary btn-sm">Back</a>
                  </div>
                        <div class="col-lg-12">
                              <form action="{{ route('store_subcategory') }}" method="post" id="myForm">
                                    @csrf
                                    <div class="card">
                                          <div class="card-body">
                                                <div class="row mb-3">
                                                      <div class="col-sm-3">
                                                            <h6 class="mb-0">Category Name</h6>
                                                      </div>
                                                      <div class="col-sm-9 text-secondary form-group">
                                                            <select name="category_id" id="category_id" class="form-control">
                                                                  <option value="">Select Category</option>
                                                                  @foreach ($allData as $item)
                                                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                                  @endforeach
                                                            </select>
                                                            @error('category_id')
                                                                  <span style="color:red">{{ $message }}</span>
                                                            @enderror
                                                      </div>
                                                </div>

                                                <div class="row mb-3">
                                                      <div class="col-sm-3">
                                                            <h6 class="mb-0">SubCategory Name</h6>
                                                      </div>
                                                      <div class="col-sm-9 text-secondary form-group">
                                                            <input type="text" name="subcategory_name" class="form-control" placeholder="SubCategory Name">
                                                            @error('subcategory_name')
                                                                  <span style="color:red">{{ $message }}</span>
                                                            @enderror
                                                      </div>
                                               </div>

                                                <div class="row">
                                                      <div class="col-sm-3"></div>
                                                      <div class="col-sm-9 text-secondary">
                                                            <input type="submit" class="btn btn-primary px-4" value="Save Changes">
                                                      </div>
                                                </div>
                                          </div>
                                    </div>
                              </form>
                        </div>
                  </div>
            </div>
      </div>
</div>
</div>

<!--===Validation JS==------>
<script type="text/javascript">
$(document).ready(function (){
  $('#myForm').validate({
      rules: {
           category_id: {
              required : true,
          }, 
          subcategory_name: {
              required : true,
          }, 
      },
      messages :{
            category_id: {
              required : 'Please Enter Category Name',
          },
          subcategory_name: {
              required : 'Please Enter SubCategory Name',
          },
      },
      errorElement : 'span', 
      errorPlacement: function (error,element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
      },
      highlight : function(element, errorClass, validClass){
          $(element).addClass('is-invalid');
      },
      unhighlight : function(element, errorClass, validClass){
          $(element).removeClass('is-invalid');
      },
  });
});

</script>

@endsection
