@extends('admin.master')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<div class="page-wrapper">
      <div class="page-content"> 
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                  <div class="breadcrumb-title pe-3">District</div>
                  <div class="ps-3">
                        <nav aria-label="breadcrumb">
                              <ol class="breadcrumb mb-0 p-0">
                                    <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">District</li>
                              </ol>
                        </nav>
                  </div>
            </div>
            <!--end breadcrumb-->
            <div class="container">
                  <div class="main-body">
                        <div class="row">
                        <div class="d-flex justify-content-between">
                              <p class="mb-0 text-uppercase">Update District</p>
                              <a href="{{ route('view_district') }}" class="mb-0 text-uppercase btn btn-primary btn-sm">Back</a>
                        </div>
    
                              <div class="col-lg-12">
                                    <form action="{{ route('update_district',$editData->id) }}" method="post" enctype="multipart/form-data">
                                          @csrf
                                          <div class="card">
                                                <div class="card-body">
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Division Name</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <select name="division_id" id="" class="form-control">
                                                                        @foreach ($divisions as $divi)
                                                                              <option value="{{$divi->id}}" {{ $divi->id == $editData->division_id? 'selected' : ''}}>{{$divi->division_name}}</option>
                                                                        @endforeach
                                                                  </select>
                                                            </div>
                                                      </div>

                                                  
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">District Name</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="district_name" class="form-control" value="{{ $editData->district_name }}">
                                                                  @error('district_name')
                                                                        <span style="color:red">{{ $message }}</span>
                                                                  @enderror
                                                            </div>
                                                      </div>


                                                      <div class="row">
                                                            <div class="col-sm-3"></div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="submit" class="btn btn-primary px-4" value="Save Changes">
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </form>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>



@endsection
