@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">District</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All District</li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->
      <div class="d-flex justify-content-between">
         <h6 class="mb-0 text-uppercase">District List</h6>
         <a class="mb-0 text-uppercase btn btn-primary btn-sm"
            data-bs-toggle="modal" data-bs-target="#addDistrict">Add District</a>
      </div>
      <hr>
      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                           <thead>
                              <tr role="row">
                                 <th>Sl</th>
                                 <th>Division Name</th>
                                 <th>District Name</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($allData as $key => $item)
                              <tr role="row" class="odd">
                                 <td>{{ $key+1 }}</td>
                                 <td>{{ $item['division']['division_name'] }}</td>
                                 <td>{{ $item->district_name }}</td>
                                 <td>
                                    <a href="{{ route('edit_district',$item->id) }}" title="edit" class="btn btn-primary btn-sm">Eidt</a>
                                    <a href="{{ route('delete_district',$item->id) }}" id="delete" title="delete" class="btn btn-danger btn-sm">Delete</a>
                                 </td>
                              </tr>
                          
                              @endforeach
                           </tbody>
                        </table>
                        {{ $allData->links('pagination_link') }}
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Add District Modal -->
<div class="modal fade" id="addDistrict" tabindex="-1" style="display: none;" aria-hidden="true">
   <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content bg-success">
         <div class="modal-header">
            <h5 class="modal-title text-white">Add District</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>
         <form action="{{ route('store_district') }}" method="post" id="MyForm">
            @csrf
            <div class="modal-body text-white">
               <div class="col-md-12">
                  <label for="division_id">Division Name</label>
                  <select name="division_id" id="division_id" class="form-control">
                        <option value="">__Select Division</option>
                        @foreach ($divisions as $division)
                              <option value="{{$division->id}}">{{$division->division_name}}</option>
                        @endforeach
                  </select>
            
                  @error('division_id')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
               </div>
               <div class="col-md-12">
                  <label for="district_name">District Name</label>
                  <input type="text" name="district_name" id="district_name" class="form-control" placeholder="Enter District Name">
                  @error('district_name')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-dark">Save changes</button>
            </div>
         </form>
      </div>
   </div>
</div>

<!--===Validation JS==------>
<script type="text/javascript">
   $(document).ready(function (){
     $('#MyForm').validate({
         rules: {
             division_id: {
                 required : true,
             }, 
             district_name: {
                 required : true,
             }, 
         },
         messages :{
            division_id: {
                 required : 'Please Enter Product Name',
             },
             district_name: {
                 required : 'Please Enter Product Name',
             },
         },
         errorElement : 'span', 
         errorPlacement: function (error,element) {
             error.addClass('invalid-feedback');
             element.closest('.form-group').append(error);
         },
         highlight : function(element, errorClass, validClass){
             $(element).addClass('is-invalid');
         },
         unhighlight : function(element, errorClass, validClass){
             $(element).removeClass('is-invalid');
         },
     });
   });
   
</script>
@endsection