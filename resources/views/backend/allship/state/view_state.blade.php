@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">State</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All State</li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->
      <div class="d-flex justify-content-between">
         <h6 class="mb-0 text-uppercase">State List</h6>
         <a class="mb-0 text-uppercase btn btn-primary btn-sm"
            data-bs-toggle="modal" data-bs-target="#addState">Add State</a>
      </div>
      <hr>
      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                           <thead>
                              <tr role="row">
                                 <th>Sl</th>
                                 <th>Division Name</th>
                                 <th>District Name</th>
                                 <th>State Name</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($allData as $key => $item)
                              <tr role="row" class="odd">
                                 <td>{{ $key+1 }}</td>
                                 <td>{{ $item['division']['division_name'] }}</td>
                                 <td>{{ $item['district']['district_name'] }}</td>
                                 <td>{{ $item->state_name }}</td>
                                 <td>
                                    <a href="{{ route('edit_state',$item->id) }}" title="edit" class="btn btn-primary btn-sm">Eidt</a>
                                    <a href="{{ route('delete_state',$item->id) }}" id="delete" title="delete" class="btn btn-danger btn-sm">Delete</a>
                                 </td>
                              </tr>
                          
                              @endforeach
                           </tbody>
                        </table>
                        {{ $allData->links('pagination_link') }}
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Add State Modal -->
<div class="modal fade" id="addState" tabindex="-1" style="display: none;" aria-hidden="true">
   <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content bg-success">
         <div class="modal-header">
            <h5 class="modal-title text-white">Add State</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>
         <form action="{{ route('store_state') }}" method="post" id="MyForm">
            @csrf
            <div class="modal-body text-white">
               <div class="col-md-12">
                  <label for="division_id">Division Name</label>
                  <select name="division_id" id="division_id" class="form-control">
                        <option value="">Select Division</option>
                        @foreach ($divisions as $division)
                              <option value="{{$division->id}}">{{$division->division_name}}</option>
                        @endforeach
                  </select>
            
                  @error('division_id')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
               </div>

               <div class="col-md-12">
                  <label for="district_id">District Name</label>
                  <select name="district_id" id="district_id" class="form-control">
                        <option value="">Select District</option>
                    
                  </select>
               </div>

               <div class="col-md-12">
                  <label for="state_name">State Name</label>
                  <input type="text" name="state_name" id="state_name" class="form-control" placeholder="Enter state Name">
                  @error('state_name')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-dark">Save changes</button>
            </div>
         </form>
      </div>
   </div>
</div>

<!--------===Show Division wise District ========------->
<script type="text/javascript">
   $(document).ready(function(){
       $('select[name="division_id"]').on('change',function(){
           var division_id = $(this).val();
           if(division_id){
               $.ajax({
                   url: "{{ url('/state/division/get/district/') }}/"+division_id,
                   type: "GET",
                   dataType:"json",
                   success:function(data){
                    $('select[name="district_id"]').html('');
   					var d =$('select[name="district_id"]').empty();
   					$.each(data, function(key, value){
   						$('select[name="district_id"]').append('<option value="'+ value.id + '">' + value.district_name + '</option>');
   					});
                   },
               });
           }else{
               alert('danger');
           }
       });
   
   });
   
</script>

<!--===Validation JS==------>
<script type="text/javascript">
   $(document).ready(function (){
     $('#MyForm').validate({
         rules: {
             division_id: {
                 required : true,
             }, 
             district_id: {
                 required : true,
             }, 
             state_name: {
                 required : true,
             }, 
         },
         messages :{
            division_id: {
                 required : 'Please Enter Product Name',
             },
             district_id: {
                 required : 'Please Enter Product Name',
             },
             state_name: {
                 required : 'Please Enter Product Name',
             },
         },
         errorElement : 'span', 
         errorPlacement: function (error,element) {
             error.addClass('invalid-feedback');
             element.closest('.form-group').append(error);
         },
         highlight : function(element, errorClass, validClass){
             $(element).addClass('is-invalid');
         },
         unhighlight : function(element, errorClass, validClass){
             $(element).removeClass('is-invalid');
         },
     });
   });
   
</script>
@endsection