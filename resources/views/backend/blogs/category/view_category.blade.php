@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Blog Category</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All Blog Category</li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->
      <div class="d-flex justify-content-between">
         <h6 class="mb-0 text-uppercase">Blog Category List</h6>
         <a class="mb-0 text-uppercase btn btn-primary btn-sm"
            data-bs-toggle="modal" data-bs-target="#addBlogCategory">Add Blog Category</a>
      </div>
      <hr>
      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                           <thead>
                              <tr role="row">
                                 <th>Sl</th>
                                 <th>Blog Category Name</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($allData as $key => $item)
                              <tr role="row" class="odd">
                                 <td>{{ $key+1 }}</td>
                                 <td>{{ $item->blog_category_name }}</td>
                                 <td>Fresh Fruit
                                 <a data-bs-toggle="modal" data-bs-target="#editBlogCategory{{$item->id}}" title="edit" class="btn btn-primary btn-sm">Eidt</a>
                                    <a href="{{ route('delete_blog_cate',$item->id) }}" id="delete" title="delete" class="btn btn-danger btn-sm">Delete</a>
                                 </td>
                              </tr>

                              <!-- Edit Blog Category -->
                              <div class="modal fade" id="editBlogCategory{{$item->id}}" tabindex="-1" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-lg modal-dialog-centered">
                                          <div class="modal-content bg-success">
                                                <div class="modal-header">
                                                      <h5 class="modal-title text-white">Edit Blog Category Name</h5>
                                                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                      <form action="{{route('update_blog_cate',$item->id)}}" method="post">
                                                            @csrf

                                                            <div class="modal-body text-white">

                                                            <div class="col-md-12">
                                                                  <label for="blog_category_name">Category Name</label>
                                                                  <input type="text" name="blog_category_name" value="{{$item->blog_category_name}}" id="blog_category_name" class="form-control">
                                                            </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                  <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                                                  <button type="submit" class="btn btn-dark">Save changes</button>
                                                            </div>
                                                      </form>
                                          </div>
                                    </div>
                              </div>
                          
                              @endforeach
                           </tbody>
                        </table>
                        {{ $allData->links('pagination_link') }}
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Add District Modal -->
<div class="modal fade" id="addBlogCategory" tabindex="-1" style="display: none;" aria-hidden="true">
   <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content bg-success">
         <div class="modal-header">
            <h5 class="modal-title text-white">Add Blog Category</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>
         <form action="{{ route('store_blog_cate') }}" method="post" id="MyForm">
            @csrf
            <div class="modal-body text-white">
              
               <div class="col-md-12">
                  <label for="blog_category_name">Blog Category  Name</label>
                  <input type="text" name="blog_category_name" id="blog_category_name" class="form-control" placeholder="Enter Blog Category Name">
                  @error('blog_category_name')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-dark">Save changes</button>
            </div>
         </form>
      </div>
   </div>
</div>

<!--===Validation JS==------>
<script type="text/javascript">
   $(document).ready(function (){
     $('#MyForm').validate({
         rules: {
             blog_category_name: {
                 required : true,
             }, 
         },
         messages :{
            blog_category_name: {
                 required : 'Please Enter Product Name',
             },
         },
         errorElement : 'span', 
         errorPlacement: function (error,element) {
             error.addClass('invalid-feedback');
             element.closest('.form-group').append(error);
         },
         highlight : function(element, errorClass, validClass){
             $(element).addClass('is-invalid');
         },
         unhighlight : function(element, errorClass, validClass){
             $(element).removeClass('is-invalid');
         },
     });
   });
   
</script>
@endsection