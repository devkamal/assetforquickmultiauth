@extends('admin.master')
@section('content')


<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Blog Post</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All Blog Post</li>
               </ol>
            </nav>
         </div>
      
      </div>
      <!--end breadcrumb-->

     <div class="d-flex justify-content-between">
            <h6 class="mb-0 text-uppercase">Blog Post List</h6>
            <a data-bs-toggle="modal" data-bs-target="#addBlogPost" class="mb-0 text-uppercase btn btn-primary btn-sm">Add Post</a>
     </div>
    
      <hr>
      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
                 
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                           <thead>
                              <tr role="row">
                                 <th>Sl</th>
                                 <th>Image</th>
                                 <th>Blog Category </th>
                                 <th>Blog Title</th>
                                 <th> Short Description</th>
                                 <th> Long Description</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($allData as $key => $item)
                                    <tr role="row" class="odd">
                                          <td class="sorting_1">{{ $key+1 }}</td>
                                          <td>
                                                <img src="{{ asset($item->blog_post_img) }}" 
                                                alt="img" width="60px;">
                                          </td>
                                          <td>{{ $item['blogcatename']['blog_category_name'] }}</td>
                                          <td>{{ Str::limit($item->blog_post_title, 10) }}</td>
                                          <td>{{ Str::limit($item->blog_post_short_desc, 20) }}</td>
                                      
                                          <td>{!! Str::words($item->blog_post_long_desc, 30,'...') !!}</td>
                                      
                                          <td>
                                                <a href="{{ route('edit_blog_post',$item->id) }}" title="edit" class="btn btn-primary btn-sm">Eidt</a>
                                                <a href="{{ route('delete_blog_post',$item->id) }}" id="delete" title="delete" class="btn btn-danger btn-sm">Delete</a>
                                          </td>
                                    </tr>
                              @endforeach
                           </tbody>
                        </table>
                        {{ $allData->links('pagination_link') }}
                     </div>
                  </div>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- Add District Modal -->
<div class="modal fade" id="addBlogPost" tabindex="-1" style="display: none;" aria-hidden="true">
   <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content bg-success">
         <div class="modal-header">
            <h5 class="modal-title text-white">Add Blog Post</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>
         <form action="{{ route('store_blog_post') }}" method="post" id="MyForm" enctype="multipart/form-data">
            @csrf
            <div class="modal-body text-white">
              
              <div class="container">
                  <div class="row">
                        <div class="col-md-6">
                              <label for="blog_category_id">Blog Category Name</label>
                              <select name="blog_category_id" id="blog_category_id" class="form-control">
                                    <option value="">Select Blog Category Name</option>
                                    @foreach ($blogAllCategories as $blog)
                                          <option value="{{ $blog->id }}">{{ $blog->blog_category_name}}</option>
                                    @endforeach
                              </select>
                        
                              @error('blog_category_id')
                              <span class="text-danger">{{$message}}</span>
                              @enderror
                        </div>

                        <div class="col-md-6">
                              <label for="blog_post_img">Blog Post  Img</label>
                              <input type="file" name="blog_post_img" id="blog_post_img" class="form-control" placeholder="Enter Blog Category Name">
                              @error('blog_post_img')
                              <span class="text-danger">{{$message}}</span>
                              @enderror
                        </div>
                  </div>

                  <div class="row">
                        <div class="col-md-6">
                              <label for="blog_post_title">Blog Post  Title</label>
                              <input type="text" name="blog_post_title" id="blog_post_title" class="form-control" placeholder="Enter Blog Category Name">
                              @error('blog_post_title')
                              <span class="text-danger">{{$message}}</span>
                              @enderror
                        </div>

                        <div class="col-md-6">
                              <label for="blog_post_short_desc">Blog Post Short Description</label>
                             <textarea name="blog_post_short_desc" id="blog_post_short_desc" class="form-control" placeholder="Write a Description here.."></textarea>
                              @error('blog_post_short_desc')
                              <span class="text-danger">{{$message}}</span>
                              @enderror
                        </div>
                  </div>

                  <div class="row">
                        <div class="col-md-12">
                              <label for="blog_post_long_desc">Blog Post  Long Description</label>
                              <textarea id="mytextarea" name="blog_post_long_desc" id="blog_post_long_desc" class="form-control" placeholder="Write a Description here.."></textarea>
                              @error('blog_post_long_desc')
                              <span class="text-danger">{{$message}}</span>
                              @enderror
                        </div>
                  </div>

              </div>

            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-dark">Save changes</button>
            </div>
         </form>
      </div>
   </div>
</div>


<!--===Validation JS==------>
<script type="text/javascript">
   $(document).ready(function (){
     $('#MyForm').validate({
         rules: {
             blog_category_name: {
                 required : true,
             }, 
             blog_post_img: {
                 required : true,
             }, 
             blog_post_title: {
                 required : true,
             }, 
             blog_post_short_desc: {
                 required : true,
             }, 
         },
         messages :{
            blog_category_name: {
                 required : 'Please Enter Product Name',
             },
             blog_post_img: {
                 required : 'Please Enter Product Name',
             },
             blog_post_title: {
                 required : 'Please Enter Product Name',
             },
             blog_post_short_desc: {
                 required : 'Please Enter Product Name',
             },
         },
         errorElement : 'span', 
         errorPlacement: function (error,element) {
             error.addClass('invalid-feedback');
             element.closest('.form-group').append(error);
         },
         highlight : function(element, errorClass, validClass){
             $(element).addClass('is-invalid');
         },
         unhighlight : function(element, errorClass, validClass){
             $(element).removeClass('is-invalid');
         },
     });
   });
   
</script>


@endsection