@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Blog Post</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All Blog Post</li>
                  <li class="breadcrumb-item active" aria-current="page"><a href="{{ url('/blogs/view-post') }}">Back</a></li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->
      <form action="{{ route('update_blog_post',$editData->id) }}" method="post" id="MyForm" enctype="multipart/form-data">
         @csrf
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <label for="blog_post_img">Blog Category name</label>
                  <select name="blog_category_id" id="blog_category_id" class="form-control">
                     <option value="">Select Category</option>
                     @foreach ($blogAllCategories as $item)
                     <option value="{{ $item->id }}" {{ $item->id == $editData->blog_category_id ? 'selected' : ''}}>{{ $item->blog_category_name }}</option>
                     @endforeach
                  </select>
                  @error('blog_category_id')
                  <span style="color:red">{{ $message }}</span>
                  @enderror
               </div>
               <div class="col-md-6">
                  <label for="blog_post_img">Blog Post  Img</label>
                  <input type="file" name="blog_post_img" id="blog_post_img" class="form-control" value="{{ $editData->blog_post_img }}"
                     placeholder="Enter Blog Category Name">
                  <input type="hidden" name="oldimg" value="{{ $editData->blog_post_img }}">
                  @error('blog_post_img')
                  <span style="color:red">{{ $message }}</span>
                  @enderror
                  <img src="{{ asset($editData->blog_post_img) }}" alt="img" width="50px">
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <label for="blog_post_title">Blog Post  Title</label>
                  <input type="text" name="blog_post_title" id="blog_post_title" value="{{ $editData->blog_post_title }}" class="form-control" placeholder="Enter Blog Category Name">
                  @error('blog_post_title')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
               </div>
               <div class="col-md-6">
                  <label for="blog_post_short_desc">Blog Post Short Description</label>
                  <textarea name="blog_post_short_desc" id="blog_post_short_desc" class="form-control" placeholder="Write a Description here..">
                  {!! $editData->blog_post_short_desc !!}
                  </textarea>
                  @error('blog_post_short_desc')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <label for="blog_post_long_desc">Blog Post  Long Description</label>
                  <textarea id="mytextarea" name="blog_post_long_desc" id="blog_post_long_desc" class="form-control" placeholder="Write a Description here..">
                  {!! $editData->blog_post_long_desc !!}
                  </textarea>
                  @error('blog_post_long_desc')
                  <span class="text-danger">{{$message}}</span>
                  @enderror
               </div>
            </div>
            <div class="row">
               <div class="col-md-12 my-4">
                  <button type="submit" class="btn btn-primary form-control">Save changes</button>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>

@endsection