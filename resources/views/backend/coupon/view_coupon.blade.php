@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Coupon</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All Coupon</li>
               </ol>
            </nav>
         </div>
      
      </div>
      <!--end breadcrumb-->

     <div class="d-flex justify-content-between">
            <h6 class="mb-0 text-uppercase">Coupon List</h6>
            <a class="mb-0 text-uppercase btn btn-primary btn-sm"
            data-bs-toggle="modal" data-bs-target="#addCoupon">Add Coupon</a>
     </div>
    
      <hr>
      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
                 
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                           <thead>
                              <tr role="row">
                                 <th>Sl</th>
                                 <th>Coupon Name</th>
                                 <th>Coupon Discount</th>
                                 <th>Coupon Validity</th>
                                 <th>Coupon Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($allData as $key => $item)
                                    <tr role="row" class="odd">
                                          <td>{{ $key+1 }}</td>
                                          <td>{{ $item->name }}</td>
                                          <td>{{ $item->discount }}</td>
                                          <td> {{ Carbon\Carbon::parse($item->validity)->format('D, d F Y') }}  </td>
                                          <td> 
                                                @if($item->validity >= Carbon\Carbon::now()->format('Y-m-d'))
                                                <span class="badge rounded-pill bg-success">Valid</span>
                                                @else
                                                <span class="badge rounded-pill bg-danger">Invalid</span>
                                                @endif
                                          </td>


                                          <td>
                                                <a data-bs-toggle="modal" data-bs-target="#editCoupon{{$item->id}}" title="edit" class="btn btn-primary btn-sm">Eidt</a>
                                                <a href="{{ route('delete_coupon',$item->id) }}" id="delete" title="delete" class="btn btn-danger btn-sm">Delete</a>
                                          </td>
                                    </tr>
                                    
                                    <!-- Edit Coupon Modal -->
                                    <div class="modal fade" id="editCoupon{{$item->id}}" tabindex="-1" style="display: none;" aria-hidden="true">
                                          <div class="modal-dialog modal-lg modal-dialog-centered">
                                                <div class="modal-content bg-success">
                                                      <div class="modal-header">
                                                            <h5 class="modal-title text-white">Edit Coupon</h5>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                      </div>
                                                            <form action="{{route('update_coupon',$item->id)}}" method="post">
                                                                  @csrf

                                                                  <div class="modal-body text-white">

                                                                  <div class="col-md-12">
                                                                        <label for="name">Coupon Name</label>
                                                                        <input type="text" name="name" value="{{$item->name}}" id="name" class="form-control">
                                                                  </div>

                                                                  <div class="col-md-12">
                                                                        <label for="validity">Coupon Validity</label>
                                                                        <input type="date" id="validity" name="validity" value="{{$item->validity}}" class="form-control" min="{{ Carbon\Carbon::now()->format('Y-m-d')}}">
                                                                  </div>

                                                                  <div class="col-md-12">
                                                                        <label for="discount">Coupon Discound</label>
                                                                        <input type="text" id="discount" name="discount" value="{{$item->discount}}" class="form-control">
                                                                  </div>

                                                                  </div>
                                                                  <div class="modal-footer">
                                                                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-dark" id="updateCoupon">Save changes</button>
                                                                  </div>
                                                            </form>
                                                </div>
                                          </div>
                                    </div>

                              @endforeach
                           </tbody>
                        </table>
                        {{ $allData->links('pagination_link') }}
                     </div>
                  </div>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<!-- Add Coupon Modal -->
<div class="modal fade" id="addCoupon" tabindex="-1" style="display: none;" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content bg-success">
                  <div class="modal-header">
                        <h5 class="modal-title text-white">Add Coupon</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
               <form action="{{ route('store_coupon') }}" method="post">
                  @csrf
                  <div class="modal-body text-white">

                    <div class="col-md-12">
                        <label for="name">Coupon Name</label>
                        <input type="text" name="name" class="form-control">
                        @error('name')
                              <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    <div class="col-md-12">
                        <label for="validity">Coupon Validity</label>
                        <input type="date" name="validity" class="form-control" min="{{ Carbon\Carbon::now()->format('Y-m-d')}}">
                        @error('validity')
                              <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    <div class="col-md-12">
                        <label for="discount">Coupon Discound</label>
                        <input type="text" name="discount" class="form-control">
                        @error('discount')
                              <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                   </div>
                   <div class="modal-footer">
                         <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                         <button type="submit" class="btn btn-dark">Save changes</button>
                   </div>
               </form>
            </div>
      </div>
</div>



 
 
@endsection