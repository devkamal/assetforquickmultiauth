@extends('admin.master')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Site Setting</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">Site Settingp</li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->
      <div class="container">
         <div class="main-body">
            <div class="row">
               <div class="col-lg-12">
                 @if ($sitesetting != '')
                  <form action="{{ route('site_setting_store',$sitesetting->id) }}" method="post" enctype="multipart/form-data">
                     @csrf
                     <div class="card">
                        <div class="card-body">
                           <div class="row mb-3">
                              <div class="col-sm-3">
                                 <h6 class="mb-0">Logo</h6>
                              </div>
                              <div class="col-sm-9 text-secondary">
                                 <input type="file" class="form-control" name="logo" id="image">
                                 <input type="hidden" class="form-control" name="oldimage" id="oldimage"  value="{{ $sitesetting->logo }}">
                              </div>
                           </div>
                           <div class="row mb-3">
                              <div class="col-sm-3">
                              </div>
                              <div class="col-sm-9">
                                 <img id="showImg" src="{{ asset($sitesetting->logo) }}" 
                                    alt="img" width="50px">
                              </div>
                           </div>
                           <div class="row mb-3">
                              <div class="col-sm-3">
                                 <h6 class="mb-0">Support Phone</h6>
                              </div>
                              <div class="col-sm-9 text-secondary">
                                 <input type="text" name="support_phone" class="form-control" value="{{ $sitesetting->support_phone }}">
                              </div>
                           </div>
                           <div class="row mb-3">
                              <div class="col-sm-3">
                                 <h6 class="mb-0">Phone One</h6>
                              </div>
                              <div class="col-sm-9 text-secondary">
                                 <input type="text" name="phone_one" class="form-control" value="{{ $sitesetting->phone_one }}">
                              </div>
                           </div>
                           <div class="row mb-3">
                              <div class="col-sm-3">
                                 <h6 class="mb-0">Email</h6>
                              </div>
                              <div class="col-sm-9 text-secondary">
                                 <input type="text" name="email" class="form-control" value="{{ $sitesetting->email }}">
                              </div>
                           </div>
                           <div class="row mb-3">
                              <div class="col-sm-3">
                                 <h6 class="mb-0">Company Address</h6>
                              </div>
                              <div class="col-sm-9 text-secondary">
                                 <input type="text" name="company_address" class="form-control" value="{{ $sitesetting->company_address }}">
                              </div>
                           </div>
                           <div class="row mb-3">
                              <div class="col-sm-3">
                                 <h6 class="mb-0">Facebook</h6>
                              </div>
                              <div class="col-sm-9 text-secondary">
                                 <input type="text" name="facebook" class="form-control" value="{{ $sitesetting->facebook }}">
                              </div>
                           </div>
                           <div class="row mb-3">
                              <div class="col-sm-3">
                                 <h6 class="mb-0">Twitter</h6>
                              </div>
                              <div class="col-sm-9 text-secondary">
                                 <input type="text" name="twitter" class="form-control" value="{{ $sitesetting->twitter }}">
                              </div>
                           </div>
                           <div class="row mb-3">
                              <div class="col-sm-3">
                                 <h6 class="mb-0">Linkedin</h6>
                              </div>
                              <div class="col-sm-9 text-secondary">
                                 <input type="text" name="linkedin" class="form-control" value="{{ $sitesetting->linkedin }}">
                              </div>
                           </div>
                           <div class="row mb-3">
                              <div class="col-sm-3">
                                 <h6 class="mb-0">Copyright</h6>
                              </div>
                              <div class="col-sm-9">
                                 <input type="text" name="copyright" class="form-control" value="{{ $sitesetting->copyright }}">
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-sm-3"></div>
                              <div class="col-sm-9 text-secondary">
                                 <input type="submit" class="btn btn-primary px-4" value="Save Changes">
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
                 @endif
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--===ShowImage==------>
<script type="text/javascript">
   $(document).ready(function(){
      $('#image').change(function(e){
         var reader = new FileReader();
         reader.onload = function(e){
            $('#showImg').attr('src',e.target.result);
         }
         reader.readAsDataURL(e.target.files['0']);
      });
   });
</script>
@endsection