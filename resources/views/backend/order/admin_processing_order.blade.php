@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Order Processing</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All Processing Order</li>
               </ol>
            </nav>
         </div>
      
      </div>
      <!--end breadcrumb-->
    
      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
                 
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                           <thead>
                              <tr role="row">
                                 <th>Sl</th>
                                 <th>Date</th>
                                 <th>Invoice</th>
                                 <th>Amount</th>
                                 <th>Payment</th>
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($allData as $key => $item)
                                    <tr role="row" class="odd">
                                          <td class="sorting_1">{{ $key+1 }}</td>
                                          <td>{{ $item->order_date }}</td>
                                          <td>{{ $item->invoice_no }}</td>
                                          <td>${{ $item->amount }}</td>
                                          <td>{{ $item->payment_method }}</td>
                                          <td><span class="text-danger">{{ $item->status }}</span></td>
                                       
                                          <td>
                                          <a href="{{ route('admin_order_details',$item->id) }}" title="details"><i class="fa fa-eye" aria-hidden="true">Details</i></a>| 
                                          <a href="{{ route('admin_invoice_download',$item->id) }}" title="invoice"><i class="fa fa-download" aria-hidden="true">Invoice</i></a>
                                          </td>
                                    </tr>
                              @endforeach
                           </tbody>
                        </table>
                        {{ $allData->links('pagination_link') }}
                     </div>
                  </div>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection