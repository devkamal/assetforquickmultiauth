@extends('admin.master')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<div class="page-wrapper">
<div class="page-content"> 
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Category</div>
            <div class="ps-3">
                  <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                              <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                              </li>
                              <li class="breadcrumb-item active" aria-current="page">Category</li>
                        </ol>
                  </nav>
            </div>
            <div class="ms-auto">
                  <div class="btn-group">
                        <button type="button" class="btn btn-primary">Settings</button>
                        <button type="button" class="btn btn-primary split-bg-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown">	<span class="visually-hidden">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-end">	<a class="dropdown-item" href="javascript:;">Action</a>
                              <a class="dropdown-item" href="javascript:;">Another action</a>
                              <a class="dropdown-item" href="javascript:;">Something else here</a>
                              <div class="dropdown-divider"></div><a class="dropdown-item" href="javascript:;">Separated link</a>
                        </div>
                  </div>
            </div>
      </div>
      <!--end breadcrumb-->
      <div class="container">
            <div class="main-body">
                  <div class="row">
                  <div class="d-flex justify-content-between">
                        <p class="mb-0 text-uppercase">Add Category</p>
                        <a href="{{ route('view_category') }}" class="mb-0 text-uppercase btn btn-primary btn-sm">Back</a>
                  </div>
                        <div class="col-lg-12">
                              <form action="{{ route('store_category') }}" method="post" enctype="multipart/form-data" id="myForm">
                                    @csrf
                                    <div class="card">
                                          <div class="card-body">
                                                <div class="row mb-3">
                                                      <div class="col-sm-3">
                                                            <h6 class="mb-0">Category Name</h6>
                                                      </div>
                                                      <div class="col-sm-9 text-secondary form-group">
                                                            <input type="text" name="name" class="form-control" placeholder="Category name">
                                                            @error('name')
                                                                  <span style="color:red">{{ $message }}</span>
                                                            @enderror
                                                      </div>
                                                </div>

                                                <div class="row mb-3">
                                                      <div class="col-sm-3">
                                                            <h6 class="mb-0">Image</h6>
                                                      </div>
                                                      <div class="col-sm-9 text-secondary">
                                                            <input type="file" class="form-control" name="image" id="image" accept=".png, .jpeg, .jpg, .svg">
                                                            @error('image')
                                                                  <span style="color:red">{{ $message }}</span>
                                                            @enderror
                                                      </div>
                                                </div>
                                                <div class="row mb-3">
                                                      <div class="col-sm-3">
                                                      </div>
                                                      <div class="col-sm-9">
                                                      <img id="showImg" src="{{ url('upload/noimg.png') }}" 
                                                      alt="img" width="50px">
                                                      </div>
                                                </div>

                                                <div class="row">
                                                      <div class="col-sm-3"></div>
                                                      <div class="col-sm-9 text-secondary">
                                                            <input type="submit" class="btn btn-primary px-4" value="Save Changes">
                                                      </div>
                                                </div>
                                          </div>
                                    </div>
                              </form>
                        </div>
                  </div>
            </div>
      </div>
</div>
</div>

<!--===ShowImage==------>
<script type="text/javascript">
$(document).ready(function(){
	$('#image').change(function(e){
		var reader = new FileReader();
		reader.onload = function(e){
			$('#showImg').attr('src',e.target.result);
		}
		reader.readAsDataURL(e.target.files['0']);
	});
});
</script>
<!--===Validation JS==------>
<script type="text/javascript">
$(document).ready(function (){
  $('#myForm').validate({
      rules: {
          name: {
              required : true,
          }, 
      },
      messages :{
          name: {
              required : 'Please Enter Category Name',
          },
      },
      errorElement : 'span', 
      errorPlacement: function (error,element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
      },
      highlight : function(element, errorClass, validClass){
          $(element).addClass('is-invalid');
      },
      unhighlight : function(element, errorClass, validClass){
          $(element).removeClass('is-invalid');
      },
  });
});

</script>

@endsection
