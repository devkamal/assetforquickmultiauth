@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Review</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All Published Review</li>
               </ol>
            </nav>
         </div>
      
      </div>
      <!--end breadcrumb-->

      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
                 
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                           <thead>
                              <tr role="row">
                                 <th>Sl</th>
                                 <th>Image</th>
                                 <th>Comment</th>
                                 <th>User Name</th>
                                 <th>Product Name</th>
                                 <th>Rating</th>
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($reviews as $key => $item)
                                    <tr role="row" class="odd">
                                          <td class="sorting_1">{{ $key+1 }}</td>
                                          <td>
                                                <img src="{{ asset($item['product']['thumbnail']) }}" alt="img" width="50px;">
                                          </td>
                                          <td>{{ Str::limit($item->comment, 25) }}</td>
                                          <td>
                                             {{ $item['user']['name']}}
                                          </td>
                                          <td>{{ $item['product']['name']}}</td>
                                          <td>
                                                @if ($item->rating == NULL)
                                                <i class="bx bxs-star text-secondary"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                @elseif ($item->rating == 1)
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                @elseif ($item->rating == 2)
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                @elseif ($item->rating == 3)
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                @elseif ($item->rating == 4)
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-secondary"></i>
                                                @elseif ($item->rating == 5)
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                @endif
                                          </td>
                                          <td>
                                                @if ($item->status == 0)
                                                      <p class="text-danger">Pending</p>
                                                      @else
                                                      <p class="text-danger">Published</p>
                                                @endif
                                          </td>
                                          <td>
                                                <a href="{{ route('approve_review',$item->id) }}" title="edit" class="btn btn-primary btn-sm">Approve</a>
                                                <a href="{{ route('delete_review',$item->id) }}" id="delete" title="edit" class="btn btn-danger btn-sm">Delete</a>
                                          </td>
                                    </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection