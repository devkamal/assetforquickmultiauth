@extends('admin.master')
@section('content')

<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">eCommerce</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">Add New Product</li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->
      <div class="card">
         <div class="card-body p-4">
            <h5 class="card-title">Add New Product</h5>
            <hr>
            <div class="form-body mt-4">
              <form action="{{ route('store_product') }}" method="post" enctype="multipart/form-data" id="MyForm">
               @csrf
              <div class="row">
                  <div class="col-lg-8">
                     <div class="border border-3 p-4 rounded">
                        <div class="mb-3">
                           <label for="name" class="form-label">Product Name</label>
                           <input type="text" name="name" class="form-control" id="name" placeholder="Enter product name">
                        </div>
                        <div class="mb-3">
                           <label for="tags" class="form-label">Product Tags</label>
                           <input type="text" name="tags" class="form-control visually-hidden" data-role="tagsinput" value="New Product, Top Product">
                        </div>
                        <div class="mb-3">
                           <label for="size" class="form-label">Product Size</label>
                           <input type="text" name="size" class="form-control visually-hidden" data-role="tagsinput" value="Small, Medium, Large">
                        </div>
                        <div class="mb-3">
                           <label for="color" class="form-label">Product Color</label>
                           <input type="text" name="color" class="form-control visually-hidden" data-role="tagsinput" value="Red, Blue, Black">
                        </div>
                        <div class="mb-3">
                           <label for="short_description" class="form-label">Short Description</label>
                           <textarea class="form-control" name="short_description" id="short_description" rows="3"></textarea>
                        </div>
                        <div class="mb-3">
                           <label for="long_description" class="form-label">Long Description</label>
                           <textarea class="form-control" name="long_description" id="mytextarea" rows="3"></textarea>
                        </div>
                        <div class="mb-3">
                           <label for="thumbnail" class="form-label">Main Thumbnail</label>
                           <input onChange="mainThumbnailUrl(this)" type="file" name="thumbnail" id="thumbnail" class="form-control">
                           <img src="" id="mainThumbnail">
                        </div>
                        <div class="mb-3">
                           <label for="inputProductDescription" class="form-label">Multiple Images</label>
                           <input type="file" name="image[]" id="multiImg" class="form-control" multiple>
                           <div class="row" id="preview_img"></div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="border border-3 p-4 rounded">
                        <div class="row g-3">
                           <div class="col-md-6">
                              <label for="price" class="form-label">Product Price</label>
                              <input type="number" name="price" class="form-control" id="price" placeholder="00.00">
                           </div>
                           <div class="col-md-6">
                              <label for="discount_price" class="form-label">Discount Price</label>
                              <input type="text" name="discount_price" class="form-control" id="discount_price" placeholder="00.00">
                           </div>
                           <div class="col-md-6">
                              <label for="code" class="form-label">Product Code</label>
                              <input type="text" name="code" class="form-control" id="code" placeholder="00.00">
                           </div>
                           <div class="col-md-6">
                              <label for="qty" class="form-label">Product QTY </label>
                              <input type="text" name="qty" class="form-control" id="qty" placeholder="00.00">
                           </div>
                         
                           <div class="col-12">
                              <label for="category">Category Name</label>
                              <select name="category_id" id="category_id" class="form-control">
                                 <option value="">Select Category</option>
                                 @foreach ($categories as $category)
                                 <option value="{{ $category->id }}">{{ $category->name }}</option>
                                 @endforeach
                              </select>
                           </div>
                           <div class="col-12">
                              <label for="subcategory_id">Sub Category</label>
                              <select name="subcategory_id" id="subcategory_id" class="form-control">
                                 <option value=""></option>
                              </select>
                           </div>
                        
                           <div class="col-md-6">
                              <div class="form-check">
                                 <input class="form-check-input" name="hot_deal" type="checkbox" value="1" id="flexCheckDefault">
                                 <label class="form-check-label" for="flexCheckDefault"> Hot Deals</label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-check">
                                 <input class="form-check-input" name="feature" type="checkbox" value="1" id="flexCheckDefault">
                                 <label class="form-check-label" for="flexCheckDefault">Featured</label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-check">
                                 <input class="form-check-input" name="speacial_offer" type="checkbox" value="1" id="flexCheckDefault">
                                 <label class="form-check-label" for="flexCheckDefault">Special Offer</label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-check">
                                 <input class="form-check-input" name="speacial_deal" type="checkbox" value="1" id="flexCheckDefault">
                                 <label class="form-check-label" for="flexCheckDefault">Special Deals</label>
                              </div>
                           </div>
                           <hr>
                           <div class="col-12">
                              <div class="d-grid">
                                 <button type="submit" class="btn btn-primary">Save Product</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--end row-->
              </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!--------===Show Thumbnail ========------->
<script type="text/javascript">
   function mainThumbnailUrl(input){
      if(input.files && input.files[0]){
         var reader = new FileReader();
         reader.onload = function(e){
            $('#mainThumbnail').attr('src',e.target.result).width(80).height(80);
         };
         reader.readAsDataURL(input.files[0]);
      }
   }
</script>
<!--------===Show MultiImage ========------->
<script> 
   $(document).ready(function(){
    $('#multiImg').on('change', function(){ //on file input change
       if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
       {
           var data = $(this)[0].files; //this file data
            
           $.each(data, function(index, file){ //loop though each file
               if(/(\.|\/)(gif|jpe?g|png|webp)$/i.test(file.type)){ //check supported file type
                   var fRead = new FileReader(); //new filereader
                   fRead.onload = (function(file){ //trigger function on successful read
                   return function(e) {
                       var img = $('<img/>').addClass('thumb').attr('src', e.target.result) .width(100)
                   .height(80); //create image element 
                       $('#preview_img').append(img); //append image to output element
                   };
                   })(file);
                   fRead.readAsDataURL(file); //URL representing the file's data.
               }
           });
            
       }else{
           alert("Your browser doesn't support File API!"); //if File API is absent
       }
    });
   });
</script>
<!--------===Show Category wise SubCategory ========------->
<script type="text/javascript">
   $(document).ready(function(){
       $('select[name="category_id"]').on('change',function(){
           var category_id = $(this).val();
           if(category_id){
               $.ajax({
                   url: "{{ url('/products/get/subcategory/') }}/"+category_id,
                   type: "GET",
                   dataType:"json",
                   success:function(data){
                    $('select[name="subcategory_id"]').html('');
   					var d =$('select[name="subcategory_id"]').empty();
   					$.each(data, function(key, value){
   						$('select[name="subcategory_id"]').append('<option value="'+ value.id + '">' + value.subcategory_name + '</option>');
   					});
                   },
               });
           }else{
               alert('danger');
           }
       });
   
   });
   
</script>

<!--===Validation JS==------>
<script type="text/javascript">
$(document).ready(function (){
  $('#MyForm').validate({
      rules: {
          name: {
              required : true,
          }, 
          short_description: {
              required : true,
          }, 
          long_description: {
              required : true,
          }, 
          thumbnail: {
              required : true,
          }, 
          price: {
              required : true,
          }, 
          discount_price: {
              required : true,
          }, 
          qty: {
              required : true,
          }, 
          code: {
              required : true,
          }, 
      },
      messages :{
          name: {
              required : 'Please Enter Product Name',
          },
          short_description: {
              required : 'Please Enter short_description',
          },
          long_description: {
              required : 'Please Enter long_description',
          },
          thumbnail: {
              required : 'Please Enter thumbnail',
          },
          price: {
              required : 'Please Enter price',
          },
          discount_price: {
              required : 'Please Enter discount_price',
          },
          qty: {
              required : 'Please Enter qty',
          },
          code: {
              required : 'Please Enter code',
          },
      },
      errorElement : 'span', 
      errorPlacement: function (error,element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
      },
      highlight : function(element, errorClass, validClass){
          $(element).addClass('is-invalid');
      },
      unhighlight : function(element, errorClass, validClass){
          $(element).removeClass('is-invalid');
      },
  });
});

</script>
@endsection