@extends('admin.master')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">eCommerce</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">Add New Product</li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->
      <div class="card">
         <div class="card-body p-4">
           <div class="justify-content-between d-flex">
               <h5 class="card-title">Update Product</h5>
               <a href="{{route('view_product')}}" class="card-title">Back</a>
           </div>
            <hr>
            <div class="form-body mt-4">
              <form action="{{ route('update_product',$editData->id) }}" method="post" enctype="multipart/form-data" id="MyForm">
               @csrf
              <div class="row">
                  <div class="col-lg-8">
                     <div class="border border-3 p-4 rounded">
                        <div class="mb-3">
                           <label for="name" class="form-label">Product Name</label>
                           <input type="text" name="name" class="form-control" id="name" placeholder="Enter product name" value="{{$editData->name}}">
                        </div>
                        <div class="mb-3">
                           <label for="tags" class="form-label">Product Tags</label>
                           <input type="text" name="tags" class="form-control visually-hidden" data-role="tagsinput" 
                           value="{{$editData->tags}}">
                        </div>
                        <div class="mb-3">
                           <label for="size" class="form-label">Product Size</label>
                           <input type="text" name="size" class="form-control visually-hidden" data-role="tagsinput" value="{{$editData->size}}">
                        </div>
                        <div class="mb-3">
                           <label for="color" class="form-label">Product Color</label>
                           <input type="text" name="color" class="form-control visually-hidden" data-role="tagsinput" value="{{$editData->color}}">
                        </div>
                        <div class="mb-3">
                           <label for="short_description" class="form-label">Short Description</label>
                           <textarea class="form-control" name="short_description" id="short_description" rows="3">
                              {{$editData->short_description}}
                           </textarea>
                        </div>
                        <div class="mb-3">
                           <label for="long_description" class="form-label">Long Description</label>
                           <textarea class="form-control" name="long_description" id="mytextarea" rows="3">
                           {{$editData->long_description}}
                           </textarea>
                        </div>

                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="border border-3 p-4 rounded">
                        <div class="row g-3">
                           <div class="col-md-6">
                              <label for="price" class="form-label">Product Price</label>
                              <input type="number" name="price" class="form-control" id="price" placeholder="00.00" value="{{$editData->price}}">
                           </div>
                           <div class="col-md-6">
                              <label for="discount_price" class="form-label">Discount Price</label>
                              <input type="text" name="discount_price" class="form-control" id="discount_price" placeholder="00.00" value="{{$editData->discount_price}}">
                           </div>
                           <div class="col-md-6">
                              <label for="code" class="form-label">Product Code</label>
                              <input type="text" name="code" class="form-control" id="code" placeholder="00.00" value="{{$editData->code}}">
                           </div>
                           <div class="col-md-6">
                              <label for="qty" class="form-label">Product QTY </label>
                              <input type="text" name="qty" class="form-control" id="qty" placeholder="00.00" value="{{$editData->qty}}">
                           </div>
                        
                           <div class="col-12">
                              <label for="category">Category Name</label>
                              <select name="category_id" id="category_id" class="form-control">
                                 <option value="">Select Category</option>
                                 @foreach ($categories as $category)
                                 <option value="{{ $category->id }}" {{ $category->id == $editData->category_id? 'selected' : ''}}>{{ $category->name }}</option>
                                 @endforeach
                              </select>
                           </div>
                           <div class="col-12">
                              <label for="subcategory_id">Sub Category</label>
                              <select name="subcategory_id" id="subcategory_id" class="form-control">
                                 @foreach ($subcate as $item)
                                    <option value="{{$item->id}}" {{ $item->id == $editData->subcategory_id ? 'selected' : ''}}>{{$item->subcategory_name}}</option>
                                 @endforeach
                                
                              </select>
                           </div>
                         
                           <div class="col-md-6">
                              <div class="form-check">
                                 <input class="form-check-input" name="hot_deal" type="checkbox" value="1" id="flexCheckDefault" 
                                 {{ $editData->hot_deal == 1 ? 'checked' : '' }}>
                                 <label class="form-check-label" for="flexCheckDefault"> Hot Deals</label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-check">
                                 <input class="form-check-input" name="feature" type="checkbox" value="1" id="flexCheckDefault" 
                                 {{$editData->feature == 1 ? 'checked' : ''}}>
                                 <label class="form-check-label" for="flexCheckDefault">Featured</label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-check">
                                 <input class="form-check-input" name="speacial_offer" type="checkbox" value="1" id="flexCheckDefault" 
                                 {{$editData->speacial_offer == 1 ? 'checked' : ''}}>
                                 <label class="form-check-label" for="flexCheckDefault">Special Offer</label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-check">
                                 <input class="form-check-input" name="speacial_deal" type="checkbox" value="1" id="flexCheckDefault" 
                                 {{$editData->speacial_deal == 1 ? 'checked' : ''}}>
                                 <label class="form-check-label" for="flexCheckDefault">Special Deals</label>
                              </div>
                           </div>
                           <hr>
                           <div class="col-12">
                              <div class="d-grid">
                                 <button type="submit" class="btn btn-primary">Save Product</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--end row-->
              </form>
            </div>
         </div>
      </div>
      <hr>
      <div class="card">
         <div class="card-header">
         <div class="title">Update main image</div>
         </div>
         <div class="card-body">
           <form action="{{route('update_mainimg',$editData->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
               <div class="col-md-6">
                  <div class="mb-3">
                     <label for="thumbnail" class="form-label">Main Thumbnail</label>
                     <input onChange="mainThumbnailUrl(this)" type="file" name="thumbnail" id="thumbnail" class="form-control" value="{{$editData->thumbnail}}">
                     <img src="{{asset($editData->thumbnail)}}" id="mainThumbnail" width="100px;">
                     <input type="hidden" name="oldimg" value="{{$editData->thumbnail}}">
                  </div>
               </div>
               <div class="col-md-6">
                  <button type="submit" class="btn btn-primary mt-4">Save Change</button>
               </div>
            </div>
           </form>
         </div>
      </div>
      <hr>

      <div class="card">
         <div class="card-header">
         <div class="title">Update Multi Images</div>
         </div>
         <div class="card-body">
            <table class="table">
               <thead>
                  <tr>
                     <th scope="col">#</th>
                     <th scope="col"> Image</th>
                     <th scope="col">Change Image</th>
                     <th scope="col">Delete</th>
                  </tr>
               </thead>
               <tbody>
                <form action="{{ route('multiimg_update') }}" method="post" enctype="multipart/form-data">
                  @csrf
                  @foreach($multiimg as $key => $img)
                  <tr>
                     <th scope="row">{{ $key+1 }}</th>
                     <td> <img src="{{ asset($img->image) }}" style="width:70; height: 40px;"> </td>
                     <td> <input type="file" class="form-group" name="image[{{ $img->id }}]"> </td>
                     <td> 
                     <input type="submit" class="btn btn-primary px-4" value="Update Image " />		
                     <a href="{{route('remove_multimg',$img->id)}}" class="btn btn-danger" id="delete"> Delete </a>		
                     </td>
                  </tr>
                  @endforeach		
                </form>
               </tbody>
            </table>
         </div>
      </div>

   </div>
</div>
<!--------===Show Thumbnail ========------->
<script type="text/javascript">
   function mainThumbnailUrl(input){
      if(input.files && input.files[0]){
         var reader = new FileReader();
         reader.onload = function(e){
            $('#mainThumbnail').attr('src',e.target.result).width(80).height(80);
         };
         reader.readAsDataURL(input.files[0]);
      }
   }
</script>
<!--------===Show MultiImage ========------->
<script> 
   $(document).ready(function(){
    $('#multiImg').on('change', function(){ //on file input change
       if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
       {
           var data = $(this)[0].files; //this file data
            
           $.each(data, function(index, file){ //loop though each file
               if(/(\.|\/)(gif|jpe?g|png|webp)$/i.test(file.type)){ //check supported file type
                   var fRead = new FileReader(); //new filereader
                   fRead.onload = (function(file){ //trigger function on successful read
                   return function(e) {
                       var img = $('<img/>').addClass('thumb').attr('src', e.target.result) .width(100)
                   .height(80); //create image element 
                       $('#preview_img').append(img); //append image to output element
                   };
                   })(file);
                   fRead.readAsDataURL(file); //URL representing the file's data.
               }
           });
            
       }else{
           alert("Your browser doesn't support File API!"); //if File API is absent
       }
    });
   });
</script>


@endsection