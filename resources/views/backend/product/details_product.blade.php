@extends('admin.master')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">eCommerce</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">Add New Product</li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->
      <div class="card">
         <div class="card-body p-4">
           <div class="justify-content-between d-flex">
               <h5 class="card-title">Update Product</h5>
               <a href="{{route('view_product')}}" class="card-title">Back</a>
           </div>
            <hr>
            <div class="form-body mt-4">
             
              <div class="row">
                  <div class="col-lg-8">
                     <div class="border border-3 p-4 rounded">
                        <div class="mb-3">
                           <label for="name" class="form-label">Product Name</label>
                           <input readonly class="form-control" id="name" placeholder="Enter product name" value="{{$details->name}}">
                        </div>
                        <div class="mb-3">
                           <label for="tags" class="form-label">Product Tags</label>
                           <input readonly class="form-control visually-hidden" data-role="tagsinput" 
                           value="{{$details->tags}}">
                        </div>
                        <div class="mb-3">
                           <label for="size" class="form-label">Product Size</label>
                           <input readonly class="form-control visually-hidden" data-role="tagsinput" value="{{$details->size}}">
                        </div>
                        <div class="mb-3">
                           <label for="color" class="form-label">Product Color</label>
                           <input readonly class="form-control visually-hidden" data-role="tagsinput" value="{{$details->color}}">
                        </div>
                        <div class="mb-3">
                           <label for="short_description" class="form-label">Short Description</label>
                           <textarea class="form-control" name="short_description" readonly rows="3">
                              {{$details->short_description}}
                           </textarea>
                        </div>
                        <div class="mb-3">
                           <label for="long_description" class="form-label">Long Description</label>
                           <textarea class="form-control" readonly id="mytextarea" rows="3">
                           {!! $details->long_description !!}
                           </textarea>
                        </div>

                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="border border-3 p-4 rounded">
                        <div class="row g-3">
                           <div class="col-md-6">
                              <label for="price" class="form-label">Product Price</label>
                              <input readonly class="form-control" id="price" placeholder="00.00" value="{{$details->price}}">
                           </div>
                           <div class="col-md-6">
                              <label for="discount_price" class="form-label">Discount Price</label>
                              <input readonly class="form-control" id="discount_price" placeholder="00.00" value="{{$details->discount_price}}">
                           </div>
                           <div class="col-md-6">
                              <label for="code" class="form-label">Product Code</label>
                              <input readonly class="form-control" id="code" placeholder="00.00" value="{{$details->code}}">
                           </div>
                           <div class="col-md-6">
                              <label for="qty" class="form-label">Product QTY </label>
                              <input readonly class="form-control" id="qty" placeholder="00.00" value="{{$details->qty}}">
                           </div>
                        
                           <div class="col-12">
                              <label for="category">Category Name</label>
                              <select readonly class="form-control">
                                 <option value="">Select Category</option>
                                 @foreach ($categories as $category)
                                 <option value="{{ $category->id }}" {{ $category->id == $details->category_id? 'selected' : ''}}>{{ $category->name }}</option>
                                 @endforeach
                              </select>
                           </div>
                           <div class="col-12">
                              <label for="subcategory_id">Sub Category</label>
                              <select readonly id="subcategory_id" class="form-control">
                                 @foreach ($subcate as $item)
                                    <option value="{{$item->id}}" {{ $item->id == $details->subcategory_id ? 'selected' : ''}}>{{$item->subcategory_name}}</option>
                                 @endforeach
                              </select>
                           </div>
                           <div class="col-12">
                              <label for="vendor_id" class="form-label">Select Vendor</label>
                              <select class="form-control" readonly>
                                 <option></option>
                                 @foreach($vendors as $item)
                                 <option value="{{ $item->id }}" {{ $item->id == $details->vendor_id ? 'selected' : ''}}>{{ $item->name }}</option>
                                 @endforeach
                              </select>
                           </div>
                           <div class="col-md-6">
                              <div class="form-check">
                                 <input class="form-check-input" name="hot_deal" type="checkbox" value="1" id="flexCheckDefault" 
                                 {{ $details->hot_deal == 1 ? 'checked' : '' }}>
                                 <label class="form-check-label" for="flexCheckDefault"> Hot Deals</label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-check">
                                 <input class="form-check-input" name="feature" type="checkbox" value="1" id="flexCheckDefault" 
                                 {{$details->feature == 1 ? 'checked' : ''}}>
                                 <label class="form-check-label" for="flexCheckDefault">Featured</label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-check">
                                 <input class="form-check-input" name="speacial_offer" type="checkbox" value="1" id="flexCheckDefault" 
                                 {{$details->speacial_offer == 1 ? 'checked' : ''}}>
                                 <label class="form-check-label" for="flexCheckDefault">Special Offer</label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-check">
                                 <input class="form-check-input" name="speacial_deal" type="checkbox" value="1" id="flexCheckDefault" 
                                 {{$details->speacial_deal == 1 ? 'checked' : ''}}>
                                 <label class="form-check-label" for="flexCheckDefault">Special Deals</label>
                              </div>
                           </div>
                      
                        </div>
                     </div>
                  </div>
               </div>
               <!--end row-->
           
            </div>
         </div>
      </div>
      <hr>
      <div class="card">
         <div class="card-header">
         <div class="title">Update main image</div>
         </div>
         <div class="card-body">
          
            <div class="row">
               <div class="col-md-6">
                  <div class="mb-3">
                     <label for="thumbnail" class="form-label">Main Thumbnail</label>
                     <img src="{{asset($details->thumbnail)}}" id="mainThumbnail" width="100px;">
                  </div>
               </div>
            </div>
       
         </div>
      </div>
      <hr>

      <div class="card">
         <div class="card-header">
         <div class="title">Update Multi Images</div>
         </div>
         <div class="card-body">
            <table class="table">
               <thead>
                  <tr>
                     <th scope="col">#</th>
                     <th scope="col"> Image</th>
                  </tr>
               </thead>
               <tbody>
          
                  @foreach($multiimg as $key => $img)
                  <tr>
                     <th scope="row">{{ $key+1 }}</th>
                     <td> <img src="{{ asset($img->image) }}" style="width:70; height: 40px;"> </td>
                  
                  </tr>
                  @endforeach		
               
               </tbody>
            </table>
         </div>
      </div>

   </div>
</div>


@endsection