@extends('admin.master')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<div class="page-wrapper">
      <div class="page-content"> 
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                  <div class="breadcrumb-title pe-3">Slider</div>
                
               
            </div>
            <!--end breadcrumb-->
            <div class="container">
                  <div class="main-body">
                        <div class="row">
                        <div class="d-flex justify-content-between">
                              <p class="mb-0 text-uppercase">Update Slider</p>
                              <a href="{{ route('view_slider') }}" class="mb-0 text-uppercase btn btn-primary btn-sm">Back</a>
                        </div>
    
                              <div class="col-lg-12">
                                    <form action="{{ route('update_slider',$editData->id) }}" method="post" enctype="multipart/form-data">
                                          @csrf
                                          <div class="card">
                                                <div class="card-body">
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Slider Title</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="title" class="form-control" value="{{ $editData->title }}">
                                                                  @error('title')
                                                                        <span style="color:red">{{ $message }}</span>
                                                                  @enderror
                                                            </div>
                                                      </div>

                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Slider Short Title</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="text" name="short_title" class="form-control" value="{{ $editData->short_title }}">
                                                                  @error('short_title')
                                                                        <span style="color:red">{{ $message }}</span>
                                                                  @enderror
                                                            </div>
                                                      </div>

                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                                  <h6 class="mb-0">Image</h6>
                                                            </div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="file" class="form-control" name="image" id="image" value="{{ $editData->image }}">
                                                                  <input type="hidden" name="old_image" value="{{ $editData->image }}">
                                                                  @error('image')
                                                                        <span style="color:red">{{ $message }}</span>
                                                                  @enderror
                                                            </div>
                                                      </div>
                                                      <div class="row mb-3">
                                                            <div class="col-sm-3">
                                                            </div>
                                                            <div class="col-sm-9">
                                                            <img id="showImg" src="{{ asset($editData->image) }}" alt="img" width="50px">
                                                            </div>
                                                      </div>

                                                      <div class="row">
                                                            <div class="col-sm-3"></div>
                                                            <div class="col-sm-9 text-secondary">
                                                                  <input type="submit" class="btn btn-primary px-4" value="Save Changes">
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </form>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>

<!--===ShowImage==------>
<script type="text/javascript">
	$(document).ready(function(){
		$('#image').change(function(e){
			var reader = new FileReader();
			reader.onload = function(e){
				$('#showImg').attr('src',e.target.result);
			}
			reader.readAsDataURL(e.target.files['0']);
		});
	});
</script>

@endsection
